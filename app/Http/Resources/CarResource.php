<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * @return array<string, mixed>
     * @param Request $request
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'make' => $this->resource->make,
            'model' => $this->resource->model,
            'year' => $this->resource->year
        ];
    }
}
