<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    /**
     * @return array<string, mixed>
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'date' => $this->resource->trip_at->format('m/d/Y'),
            'miles' => $this->resource->miles,
            'total' => $this->resource->total_miles(),
            'car' => new CarResource($this->resource->car)
        ];
    }
}
