<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;

class DetailedCarResource extends CarResource
{
    /**
     * @return array<string, mixed>
     * @param Request $request
     */
    public function toArray($request): array
    {
        $baseInfo = parent::toArray($request);

        return array_replace($baseInfo, [
            'trip_count' => $this->resource->trip_count(),
            'trip_miles' => $this->resource->trip_miles()
        ]);
    }
}
