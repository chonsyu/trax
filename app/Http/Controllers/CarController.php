<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarResource;
use App\Http\Resources\DetailedCarResource;
use App\Model\Car;
use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CarController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Car::class, 'car');
    }

    public function index(): Response
    {
        /** @var User $user */
        $user = Auth::user();

        $result = Car::query()
            ->where('owner_id', '=', $user->id)
            ->get();

        $resource = CarResource::collection($result);

        return $resource->response();
    }

    public function store(Request $request): Response
    {
        $request->validate([
            'year' => 'required|integer|digits:4',
            'make' => 'required|string',
            'model' => 'required|string'
        ]);

        /** @var User $user */
        $user = Auth::user();

        $year = $request->request->getInt('year');
        $make = $request->request->get('make');
        $model = $request->request->get('model');

        $car = new Car();

        $car->owner_id = $user->id;
        $car->year = $year;
        $car->make = $make;
        $car->model = $model;

        $car->push();

        return new JsonResponse(['success' => true]);
    }

    public function show(Car $car): Response
    {
        $resource = new DetailedCarResource($car);
        return $resource->response();
    }

    public function destroy(Car $car): Response
    {
        $car->trips()->delete();
        $car->delete();
        return new JsonResponse(['success' => true]);
    }
}
