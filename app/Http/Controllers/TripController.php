<?php

namespace App\Http\Controllers;

use App\Http\Resources\TripResource;
use App\Model\Trip;
use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class TripController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Trip::class, 'trip');
    }

    public function index(): Response
    {
        /** @var User $user */
        $user = Auth::user();

        $result = Trip::query()
            ->where('user_id', '=', $user->id)
            ->get();

        $resource = TripResource::collection($result);

        return $resource->response();
    }

    public function store(Request $request): Response
    {
        $request->validate([
            'date' => 'required|date', // ISO 8601 string
            'car_id' => 'required|integer:exists:App\Models\Car,id',
            'miles' => 'required|numeric'
        ]);

        /** @var User $user */
        $user = Auth::user();

        $carId = $request->request->getInt('car_id');
        $date = $request->request->get('date');
        $miles = $request->request->getInt('miles');

        $trip = new Trip();

        $trip->miles = $miles;
        $trip->car_id = $carId;
        $trip->trip_at = Carbon::parse($date);
        $trip->user_id = $user->id;

        $trip->push();

        return new JsonResponse(['success' => true]);
    }
}
