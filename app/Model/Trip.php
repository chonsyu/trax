<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Model\Trip
 *
 * @property int $id
 * @property int $miles
 * @property int $user_id
 * @property int $car_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Trip newModelQuery()
 * @method static Builder|Trip newQuery()
 * @method static Builder|Trip query()
 * @method static Builder|Trip whereCarId($value)
 * @method static Builder|Trip whereCreatedAt($value)
 * @method static Builder|Trip whereId($value)
 * @method static Builder|Trip whereMiles($value)
 * @method static Builder|Trip whereUpdatedAt($value)
 * @method static Builder|Trip whereUserId($value)
 * @property Carbon|null $deleted_at
 * @method static Builder|Trip whereDeletedAt($value)
 * @property Carbon $trip_at
 * @property-read Car $car
 * @method static Builder|Trip onlyTrashed()
 * @method static Builder|Trip whereTripAt($value)
 * @method static Builder|Trip withTrashed()
 * @method static Builder|Trip withoutTrashed()
 */
class Trip extends Model
{
    use SoftDeletes, HasFactory;

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'trip_at' => 'date'
    ];

    protected $table = 'trips';

    public function car(): BelongsTo {
        return $this->belongsTo(Car::class);
    }

    public function total_miles(): float {
        return Trip::query()
            ->where('trip_at', '<=', $this->created_at)
            ->sum('miles');
    }
}
