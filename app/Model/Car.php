<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Model\Car
 *
 * @property int $id
 * @property string $make
 * @property string $model
 * @property int $year
 * @property int $owner_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Car newModelQuery()
 * @method static Builder|Car newQuery()
 * @method static Builder|Car query()
 * @method static Builder|Car whereCreatedAt($value)
 * @method static Builder|Car whereId($value)
 * @method static Builder|Car whereMake($value)
 * @method static Builder|Car whereModel($value)
 * @method static Builder|Car whereOwnerId($value)
 * @method static Builder|Car whereUpdatedAt($value)
 * @method static Builder|Car whereYear($value)
 * @property string|null $deleted_at
 * @method static Builder|Car whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Car onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Car withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Car withoutTrashed()
 */
class Car extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'cars';

    public function trips(): HasMany
    {
        return $this->hasMany(Trip::class, 'car_id', 'id');
    }

    public function trip_count(): int {
        return $this->trips()->count();
    }

    public function trip_miles(): float {
        return $this->trips()->sum('miles');
    }
}
