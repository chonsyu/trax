<?php

namespace App\Providers;

use App\Model\Car;
use App\Model\Trip;
use App\Policies\CarPolicy;
use App\Policies\TripPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * @var array<string, string>
     */
    protected $policies = [
        Car::class => CarPolicy::class,
        Trip::class => TripPolicy::class
    ];

    public function boot(): void
    {
        $this->registerPolicies();
        Passport::routes();
    }
}
