<?php

namespace App\Policies;

use App\Model\Trip;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TripPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Trip $trip): bool
    {
        return $trip->user_id === $user->id;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Trip $trip): bool
    {
        return $trip->user_id === $user->id;
    }

    public function delete(User $user, Trip $trip): bool
    {
        return $trip->user_id === $user->id;
    }

    public function restore(User $user, Trip $trip): bool
    {
        return $trip->user_id === $user->id;
    }

    public function forceDelete(User $user, Trip $trip): bool
    {
        return $trip->user_id === $user->id;
    }
}
