<?php

namespace App\Policies;

use App\Model\Car;
use App\Model\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Car $car): bool
    {
        return $car->owner_id === $user->id;
    }

    public function create(User $user): bool
    {
        return true;
    }

    public function update(User $user, Car $car): bool
    {
        return $car->owner_id === $user->id;
    }

    public function delete(User $user, Car $car): bool
    {
        return $car->owner_id === $user->id;
    }

    public function restore(User $user, Car $car): bool
    {
        return $car->owner_id === $user->id;
    }

    public function forceDelete(User $user, Car $car): bool
    {
        return $car->owner_id === $user->id;
    }
}
