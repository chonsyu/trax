<?php

namespace Tests\Feature;

use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class TripControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_trips_list_without_access(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 2]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get('/api/trips');

        $response
            ->assertStatus(200)
            ->assertExactJson(['data' => []]);
    }

    public function test_trips_list_with_access(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get('/api/trips');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    0 => [
                        'id',
                        'miles',
                        'total',
                        'car' => [
                            'id',
                            'make',
                            'model',
                            'year'
                        ]
                    ]
                ]
            ]
        );
    }

    public function test_add_trip(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->post('/api/trips', [
                'car_id' => 1,
                'date' => Carbon::now()->toIso8601String(),
                'miles' => 300
            ]);

        $response
            ->assertStatus(200)
            ->assertExactJson(['success' => true]);
    }
}
