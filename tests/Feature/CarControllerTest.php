<?php

namespace Tests\Feature;

use App\Model\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CarControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_cars_list_without_access(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 2]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get('/api/cars');

        $response
            ->assertStatus(200)
            ->assertExactJson(['data' => []]);
    }

    public function test_cars_list_with_access(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get('/api/cars');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                    'data' => [
                        0 => [
                            'id',
                            'make',
                            'model',
                            'year'
                        ]
                    ]
                ]
            );
    }

    public function test_add_car(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->post('/api/cars', [
                'year' => 1994,
                'make' => 'Volkswagen',
                'model' => 'Golf'
            ]);

        $response
            ->assertStatus(200)
            ->assertExactJson(['success' => true]);
    }

    public function test_get_car(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->get('/api/cars/2');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'year',
                    'make',
                    'model',
                    'trip_count',
                    'trip_miles'
                ]
            ]);
    }

    public function test_delete_car(): void
    {
        /** @var User $user */
        $user = User::factory()->make(['id' => 1]);

        $response = $this
            ->actingAs($user, 'api')
            ->withHeaders([
                'Accept' => 'application/json'
            ])
            ->delete('/api/cars/1');

        $response
            ->assertStatus(200)
            ->assertExactJson(['success' => true]);
    }
}
