<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:wipe');
        $this->artisan('migrate', ['--database' => 'mysql_test', '--seed' => true]);
    }
}
