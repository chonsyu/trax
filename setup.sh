#!/bin/zsh

git submodule update --init --recursive
cp laradock-env laradock/.env
cp createdb.sql laradock/mariadb/docker-entrypoint-initdb.d/createdb.sql
cp .env.example .env

cd laradock
docker-compose build --no-cache nginx workspace mariadb
docker-compose up -d nginx mariadb workspace
docker-compose exec workspace composer install
docker-compose exec workspace npm ci
docker-compose exec workspace php artisan migrate
docker-compose exec workspace npm run watch
