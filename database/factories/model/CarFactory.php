<?php
namespace Database\Factories\Model;

use App\Model\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    protected $model = Car::class;

    public function definition(): array
    {
        return [
            'make' => ucfirst($this->faker->word),
            'model' => ucfirst($this->faker->word),
            'year' => $this->faker->year,
            'owner_id' => 1,
        ];
    }
}
