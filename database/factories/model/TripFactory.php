<?php
namespace Database\Factories\Model;

use App\Model\Trip;
use Database\Seeders\CarsTableSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;

class TripFactory extends Factory
{
    protected $model = Trip::class;

    public function definition(): array
    {
        return [
            'trip_at' => $this->faker->dateTimeThisMonth,
            'miles' => $this->faker->numberBetween(0, 100),
            'car_id' => $this->faker->numberBetween(1, CarsTableSeeder::FAKE_CARS_COUNT),
            'user_id' => 1,
        ];
    }
}
