<?php

namespace Database\Seeders;

use App\Model\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run(): void
    {
        $user = User::factory()->make();
        $user->push();
    }
}
