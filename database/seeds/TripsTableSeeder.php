<?php

namespace Database\Seeders;

use App\Model\Trip;
use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
    const FAKE_TRIPS_COUNT = 5;

    public function run(): void
    {
        for ($i = 0; $i < self::FAKE_TRIPS_COUNT; $i++) {
            $trip = Trip::factory()->make();
            $trip->push();
        }
    }
}
