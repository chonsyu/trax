<?php

namespace Database\Seeders;

use App\Model\Car;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    const FAKE_CARS_COUNT = 5;

    public function run()
    {
        for ($i = 0; $i < self::FAKE_CARS_COUNT; $i++) {
            $car = Car::factory()->make();
            $car->push();
        }
    }
}
